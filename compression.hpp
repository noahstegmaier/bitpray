#pragma once
#include <filesystem>
namespace fs = std::filesystem;
#include <optional>
#include <vector>
#include <common/saneints.hpp>

enum class error {None, OutOfMemory, whatever};

error compressFile(fs::path input, fs::path output);
//error decompressFile(fs::path input, std::optional<fs::path> directory);
error decompressFile(fs::path input, fs::path output);

error compress(const std::vector<u8>& in, std::vector<u8>& out);
error compress2(const std::vector<u8>& in, std::vector<u8>& out);

error decompress(const std::vector<u8>& in, std::vector<u8>& out);
error decompress2(const std::vector<u8>& in, std::vector<u8>& out);
