#include "compression.hpp"
#include <cassert>
#include <cmath>


#include <bit>
#include <numeric>
#include <algorithm>
#include <limits>
#include <cctype>

// debugging
#include <fmt/core.h>

#include "common/fshelpers.hpp"
#include "bitpray/bitaccess.hpp"
#include "bitpray/bitpray.hpp"

using namespace std;


error compressFile(fs::path input, fs::path output) {
    vector<u8> in, out;
    if (readWholeFile(input, in) == 0) {
        return error::whatever;
    }
    error e = compress2(in, out);
    if (e == error::None) {
        bool success = writeWholeFile(output, out);
        if (!success) {
            return error::whatever;
        }
    }
    return e;
}

error decompressFile(fs::path input, fs::path output) {
    vector<u8> in, out;
    if (readWholeFile(input, in) == 0) {
        return error::whatever;
    }
    error e = decompress2(in, out);
    if (e == error::None) {
        bool success = writeWholeFile(output, out);
        if (!success) {
            return error::whatever;
        }
    }
    return error::None;
}

error compress2(const vector<u8> &in, vector<u8> &out) {
    vector<u64> counts(256);
    vector<u64> map(256, numeric_limits<u64>::max());
    vector<u8> reversemap;
    reversemap.reserve(256);
    for (auto &e : in) {
        counts[e] += 1;
    }
    u64 count = 0;
    for (u64 i = 0; i < counts.size(); ++i) {
        if (counts[i] != 0) {
            map[i] = count;
            reversemap.push_back(i);
            count += 1;
        }
    }
    u64 bits = bitpray::neededBits(count - 1);
    u64 tripleSize = bitpray::neededBits((count - 1) * (count - 1) * (count - 1));
    u64 nonTupleElements = in.size() % 3;
    u64 expectedNaiveSizeBits = tripleSize * (in.size() / 3) + nonTupleElements * bits;
    u64 expectedNaiveSize = (expectedNaiveSizeBits + 7) / 8;
    // 1 byte format, 1 byte map size, 4 bit tuple size, 4 bit overhead, map, data
    u64 realsize = 3 + count + (expectedNaiveSizeBits + 7) / 8;
    u64 overhead = 8 - (expectedNaiveSizeBits % 8);
    fmt::print("{} symbols\n", count);
    fmt::print("{} bits per symbol\n", bits);
    fmt::print("{} bits per 3 symbols\n", tripleSize);
    fmt::print("{} bytes needed in triples\n", expectedNaiveSize);
    fmt::print("{} elements not in triples\n", nonTupleElements);
    fmt::print("{} bits overhead\n", overhead);
    out.resize(realsize);
    out[0] = 2;
    out[1] = count;
    out[2] = (nonTupleElements << 4) | overhead;
    for (u64 i = 0; i < reversemap.size(); ++i) {
        out[3 + i] = reversemap[i];
    }
    bitpusher p(out, 3 + count);
    for (u64 i = 0; i < in.size() / 3; ++i) {
        u64 start = i * tripleSize;
        u64 end = i * tripleSize + tripleSize - 1;
        u8 i1 = map[in[i * 3]];
        u8 i2 = map[in[i * 3 + 1]];
        u8 i3 = map[in[i * 3 + 2]];
        u64 temp = bitpray::encode3to1(count - 1, count - 1, count - 1, i1, i2, i3);
        p(start, end, temp);
    }
    // TODO: deal with the overhead
    fmt::print("{} bytes written\n", out.size());
    return error::None;
}

error compress(const vector<u8> &in, vector<u8> &out) {
    fmt::print("{} bytes read\n", in.size());
    vector<u64> counts(256);
    vector<u64> map(256, numeric_limits<u64>::max());
    vector<u8> reversemap;
    reversemap.reserve(256);
    for (auto &e : in) {
        counts[e] += 1;
    }
    u64 count = 0;
    // fmt::print("character map:\n");
    for (u64 i = 0; i < counts.size(); ++i) {
        if (counts[i] != 0) {
            map[i] = count;
            reversemap.push_back(i);
            // string toprint;
            // if (isprint(i)) {
            //     toprint = char(i);
            // } else {
            //     toprint = "idk";
            // }
            // fmt::print("\t{} - {} : {}\n", i, toprint, count);
            count += 1;
        }
    }

    u64 bits = bitpray::neededBits(count - 1);
    u64 expectedNaiveSize = (bits * in.size() + 7) / 8;
    // we need to save the number of chars in the map (in 1 byte)+ the character map
    u64 realsize = 1 + count + expectedNaiveSize;
    out.resize(realsize);
    out[0] = count;
    for (u64 i = 0; i < reversemap.size(); ++i) {
        out[1 + i] = reversemap[i];
    }
    fmt::print("{} symbols\n", count);
    fmt::print("{} bits needed\n", bits);
    fmt::print("{} bytes needed\n", (bits * in.size() + 7) / 8);
    fmt::print("{} bytes needed in triples\n",
               (bitpray::neededBits((count - 1) * (count - 1) * (count - 1)) * in.size() / 3 + 7) / 8);
    fmt::print("{} bytes needed in 8-tuples\n",
               (bitpray::neededBits((count - 1) * (count - 1) * (count - 1) * (count - 1) * (count - 1) *
                           (count - 1) * (count - 1) * (count - 1)) *
                    in.size() / 8 +
                7) /
                   8);

    bitpusher p(out, 1 + count);
    u64 start = 0;
    for (const auto &e : in) {
        p(start, start + bits - 1, map[e]);
        start += bits;
    }
    fmt::print("{} bits overhead!\n", expectedNaiveSize * 8 - start);
    fmt::print("{} bytes written\n", out.size());
    return error::None;
}
void calcdistances(const vector<u8> &in) {
    vector<u64> counts(256);
    for (auto &e : in) {
        counts[e] += 1;
    }
    vector<size_t> idx(counts.size());
    iota(idx.begin(), idx.end(), 0);

    sort(idx.begin(), idx.end(),
         [&counts](size_t i1, size_t i2) { return counts[i1] < counts[i2]; });
    vector<u64> lastseen(256, numeric_limits<u64>::max());
    vector<u64> mindist(256, numeric_limits<u64>::max());
    vector<u64> maxdist(256, numeric_limits<u64>::min());
    vector<u64> totaldist(256, 0);
    for (u64 i = 0; i < in.size(); ++i) {
        if (lastseen[in[i]] != numeric_limits<u64>::max()) {
            u64 dist = i - lastseen[in[i]];
            if (dist > maxdist[in[i]]) {
                maxdist[in[i]] = dist;
            }
            if (dist < mindist[in[i]]) {
                mindist[in[i]] = dist;
            }
            totaldist[in[i]] += dist;
        }
        lastseen[in[i]] = i;
    }
    for (const auto &e : idx) {
        if (counts[e] != 0) {
            fmt::print("value: {}, count: {}, mindist: {}, maxdist: {}, avgdist: {}\n", e,
                       counts[e], mindist[e], maxdist[e], float(totaldist[e]) / counts[e]);
        }
    }
}

error decompress(const vector<u8> &in, vector<u8> &out) {
    u64 count = in[0];
    vector<u8> map(count);
    for (u64 i = 0; i < count; ++i) {
        map[i] = in[1 + i];
    }
    // TODO check if there are duplicates?
    u64 bits = bitpray::neededBits(count - 1);
    u64 expectedNaiveSize = in.size() - count - 1;
    fmt::print("{} symbols\n", count);
    u64 expectedBytes = expectedNaiveSize * 8 / bits;
    out.resize(expectedBytes);
    fmt::print("{} bytes uncompressed\n", expectedBytes);
    bitpuller p(in, 1 + count);
    u64 i;
    for (i = 0; i < expectedBytes; ++i) {
        u64 temp = p(i * bits, i * bits + bits - 1);
        out[i] = map[temp];
    }
    fmt::print("{} bits overhead!\n", expectedNaiveSize * 8 - i);
    return error::None;
}

error decompress2(const vector<u8> &in, vector<u8> &out) {
    u8 format = in[0];
    u8 count = in[1];
    u8 overhead = in[2] & 0xf;
    u8 nonTupleElements = (in[2] >> 4) & 0xf;
    u8 tupleSize = 3;
    vector<u8> map(count);
    for (u64 i = 0; i < count; ++i) {
        map[i] = in[3 + i];
    }
    // size of encoded stuff
    u64 expectedNaiveSize = in.size() - count - 3;
    // TODO check if there are duplicates?
    u64 bits = bitpray::neededBits(count - 1);
    u64 tripleSize = bitpray::neededBits((count - 1) * (count - 1) * (count - 1));
    // size of the output
    u64 expectedBytes =
        (expectedNaiveSize * 8 - overhead - nonTupleElements * bits) / tripleSize * 3 +
        nonTupleElements;
    u64 cursedShit = (expectedNaiveSize * 8 - overhead - nonTupleElements * bits) % tripleSize;

    fmt::print("{} symbols\n", count);
    fmt::print("{} bytes uncompressed\n", expectedBytes);
    fmt::print("{} elements not in triples\n", nonTupleElements);
    fmt::print("{} bits overhead\n", overhead);
    assert(cursedShit == 0);
    out.resize(expectedBytes);
    bitpuller p(in, 3 + count);
    u64 i;
    for (i = 0; i < expectedBytes / 3; ++i) {
        u64 temp = p(i * tripleSize, i * tripleSize + tripleSize - 1);
        auto [i1, i2, i3] = bitpray::decode1to3(count - 1, count - 1, count - 1, temp);
        out[i * 3 + 0] = map[i1];
        out[i * 3 + 1] = map[i2];
        out[i * 3 + 2] = map[i3];
    }

    return error::None;
}
