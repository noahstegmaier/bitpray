# bitpray

Proof of concept for compressing Data that doesnt need full bits
Example: you have an enum with 5 possible states: naivly you wood need 3 bits to save one enum or 9 bits for 3, but with some simple arithmetic you can save 3 of those enums in 7 bits, saving 22%

## TODO
It should be possible to have the size of one variable to depend on the value of the previous one.
A similar thing should work for typed enums/std::variant/sum types/whatever you want to call it.

## License
GPL3, for now
