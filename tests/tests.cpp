#include "gtest/gtest.h"
#include "bitpray/bitaccess.hpp"
#include <bitpray/bitpray.hpp>
#include <vector>

using namespace std;

TEST(bitaccess, 8bit) {
    vector<u8> content = {0b00011111, 0b01000000};
    // 0001111101000000
    bitpuller p(content);
    ASSERT_EQ(p(0, 7), 0b000011111);
    ASSERT_EQ(p(1, 8), 0b000111110);
    ASSERT_EQ(p(2, 9), 0b001111101);
    ASSERT_EQ(p(3, 10), 0b11111010);
    ASSERT_EQ(p(4, 11), 0b11110100);
    ASSERT_EQ(p(5, 12), 0b11101000);
    ASSERT_EQ(p(6, 13), 0b11010000);
    ASSERT_EQ(p(7, 14), 0b10100000);
    ASSERT_EQ(p(8, 15), 0b01000000);
}

TEST(bitaccess, span2) {
    vector<u8> content(3);
    bitpusher p(content);
    p(7, 16, 0b1111111111);
    ASSERT_EQ(p(7, 16), 0b1111111111);
    ASSERT_EQ(content[0], 0b00000001);
    ASSERT_EQ(content[1], 0b11111111);
    ASSERT_EQ(content[2], 0b10000000);
    ASSERT_EQ(p(0, 7), 0b00000001);
    ASSERT_EQ(p(8, 15), 0b11111111);
    ASSERT_EQ(p(16, 23), 0b10000000);
}

TEST(bitaccess, span3) {
    vector<u8> content = {0b01010101, 0b10101010, 0b00000000, 0b11111111};
    const vector<u8> expectedContent = {0b01010100, 0b11110101, 0b11111010, 0b01111111};
    bitpusher p(content);
    p(7, 24, 0b011110101111110100);
    ASSERT_EQ(p(7, 24), 0b011110101111110100);
    ASSERT_EQ(content, expectedContent);
}

TEST(bitaccess, edgecases) {
    vector<u8> content(9);
    const vector<u8> expectedContent = {0b10000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
                                        0b00000000, 0b00000000, 0b00000000, 0b00000000};
    const vector<u8> expectedContent2 = {0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111,
                                         0b11111111, 0b11111111, 0b00000000, 0b00000000};
    const vector<u8> nullVector(9);

    bitpusher p(content);
    p(0, 55, 1ul << 55);
    ASSERT_EQ(content, expectedContent);
    p(0, 55, (1ul << 56) - 1);
    ASSERT_EQ(content, expectedContent2);
    p(0, 55, 0);
    ASSERT_EQ(content, nullVector);
    p(12, 67, (1ul << 56) - 1);
    ASSERT_EQ(p(12, 67), (1ul << 56) - 1);
    ASSERT_EQ(p(8, 15), 0b00001111);
    ASSERT_EQ(p(64, 71), 0b11110000);
    ASSERT_EQ(p(12, 19), 0b11111111);
    ASSERT_EQ(p(21, 28), 0b11111111);
    ASSERT_EQ(p(30, 37), 0b11111111);
    ASSERT_EQ(p(39, 46), 0b11111111);
}

TEST(bitpray, compare3anddyn) {
    using namespace bitpray;
    u16 a = 7, b = 11, c = 4;
    u16 max = 11;
    u64 r1 = encode3to1(11, 11, 11, a, b, c);
    u64 r2 = encode({11, 11, 11}, {a, b, c});
    ASSERT_EQ(r1, r2);
    auto [d, e, f]  = decode1to3(11, 11, 11, r1);
    auto dec2 = decode({11, 11, 11}, r1);
    ASSERT_EQ(vector<u16>({d, e, f}), dec2);
    ASSERT_EQ(a, d);
    ASSERT_EQ(b, e);
    ASSERT_EQ(c, f);
    vector<u16> testValues {5,8,1,0,9,6};
    u64 e1 = encode(9,testValues);
    u64 e2 = encode({9,9,9,9,9,9},testValues);
    ASSERT_EQ(e1, e2);
    auto d1 = decode(6,9,e1);
    auto d2 = decode({9,9,9,9,9,9},e1);
    ASSERT_EQ(d1, d2);
}

TEST(bitpray, neededBits) {
    ASSERT_EQ(bitpray::neededBits(3), 2);
    ASSERT_EQ(bitpray::neededBits(15), 4);
    ASSERT_EQ(bitpray::neededBits(4), 3);
    ASSERT_EQ(bitpray::neededBits(2, 46339), 31);
    ASSERT_EQ(bitpray::neededBits(17,2), 27);
    ASSERT_EQ(bitpray::neededBits(20,2), 32);
    ASSERT_EQ(bitpray::neededBits(40,2), 64);
    ASSERT_EQ(bitpray::neededBits({4,5,16}), 9);
    ASSERT_EQ(bitpray::neededBits({7,11,4}), 9);
}