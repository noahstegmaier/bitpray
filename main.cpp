#include <cstdint>
#include <fmt/compile.h>
#include <optional>
#include <tuple>
#include <string>
#include <vector>
#include "common/saneints.hpp"
#include <filesystem>
#include <string_view>

#include "compression.hpp"

using namespace std;

void printHelp() {
	fmt::print("bp <command> <archive_name> [<file_name>]\n");
	fmt::print("<Commands>\n");
	fmt::print("  c : compress file to archive\n");
	fmt::print("  d : decommpress archive\n");
	fmt::print("  h : show help\n");
}



int main(int argCount, const char *argv[]) {
	if (argCount >= 2) {
		string_view arg{argv[1]};
		if (arg == "c") {
			if (argCount >= 4) {
				compressFile(argv[3], argv[2]);
			} else {
				printHelp();
				return 1;
			}
		} else if (arg == "d") {
			if (argCount >= 4) {
				//optional<fs::path> dir;
				decompressFile(argv[3], argv[2]);
			} else {
				printHelp();
				return 1;
			}
		} else if (arg == "h") {
			printHelp();
			return 0;
		} else {
			printHelp();
			return 1;
		}
	} else {
		printHelp();
		return 1;
	}
}
