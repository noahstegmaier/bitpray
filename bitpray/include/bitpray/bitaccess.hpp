#pragma once
#include <vector>
#include "common/saneints.hpp"

// Class to acces a vector of u8's like a vector of bits.
// convention:
// bytes are read byte by byte(so big endian), the bit with index 0 is the most significant bit of a
// byte
class bitpusher {
  public:
    bitpusher(std::vector<u8> &data, u64 offset = 0);
    // read bits from index start to including end and return them as a 64 bit uint.
    // obviously can't be more than 64 bits
    u64 operator()(u64 start, u64 end) const;
    // write up to 52 least significant bits of value to index start to including end
    // obviously can't be more than 64 bits, but due to naive implemention this will not work when
    // value < 0x00ffffffffffffff
    void operator()(u64 start, u64 end, u64 value);
    void print() const;

  private:
    std::vector<u8> &data;
    const u64 offset;
};

// copy of bitpusher without the write function so that it can be used for const vectors
class bitpuller {
  public:
    bitpuller(const std::vector<u8> &data, u64 offset = 0);
    // read bits from index start to including end and return them as a 64 bit uint.
    // obviously can't be more than 64 bits
    u64 operator()(u64 start, u64 end) const;
    void print() const;

  private:
    const std::vector<u8> &data;
    const u64 offset;
};