#pragma once
#include <common/saneints.hpp>
#include <tuple>
#include <vector>

// TODO: Descibe the algorithm
// the maximum amount of variables that is possible to encode are 40 variables with 3 possible
// states.
// Examples of savings:
// number of variables * number of states: savings
// 40 * 3   : 20%
// 3 * 5    : 22%
// 2 * 46340: 3.1%
// 4 * 38967: 4.6%
// 3 * 11   : 8.3%
// 6 * 11   : 12.5%
// The inputs are limited to 16 bit because of diminishing returns, consider that you usually you
// need to save metadata like the size and maxvalues of the encoded value so even 3.1% are not worth
// the effort.
// In practice it probably only makes sense to encode where the digits after the dot of
// log2(count) are >.0 and <.5, unless I find a way to make this more effizient to encode for bigger
// values that does not depend on interger division.

// Note: the last value has to be decoded first, theoretically the maxvalue of the second to last
// value cold depend on the encoded lastvalue

namespace bitpray {
std::tuple<u8, u8, u8> decode3x5(u8 in);

u32 encode3to1(u8 max1, u8 max2, u8 max3, u8 i1, u8 i2, u8 i3);

std::tuple<u8, u8, u8> decode1to3(u8 max1, u8 max2, u8 max3, u32 in);

std::vector<u16> decode(std::vector<u16> maxValues, u64 in);

std::vector<u16> decode(u8 size, u16 maxValue, u64 in);

u64 encode(std::vector<u16> maxValues, std::vector<u16> values);

u64 encode(u16 maxValue, std::vector<u16> values);

// Amount of bits needed for a an integer with the maximum value of maxvalue
// pass n-1 if you have a n = amount of symbols.
u64 neededBits(u64 maxValue);

// Amount of bits needed for size integers with the maximum value of maxvalue
// pass n-1 if you have a n = amount of symbols.
u64 neededBits(u8 size, u16 maxValue);

// Amount of bits needed for the given array of maximum values
// pass n-1 if you have a n = amount of symbols.
u64 neededBits(std::vector<u16> maxValues);
} // namespace bitpray
