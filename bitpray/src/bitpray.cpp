#include "bitpray.hpp"
#include <cassert>
using namespace std;

namespace bitpray {
// assume most significant bit isn't used
tuple<u8, u8, u8> decode3x5(u8 in) {
    tuple<u8, u8, u8> r;
    get<2>(r) = in % 5;
    in /= 5;
    get<1>(r) = in % 5;
    in /= 5;
    get<0>(r) = in;
    return r;
}

// assume values go from 0 to max-1
// max1 technically not needed
u32 encode3to1(u8 max1, u8 max2, u8 max3, u8 i1, u8 i2, u8 i3) {
    u32 r = i3;
    r += i2 * (max3 + 1);
    r += i1 * (max3 + 1) * (max2 + 1);
    return r;
}

// assume values go from 0 to max-1
// max1 technically not needed
tuple<u8, u8, u8> decode1to3(u8 max1, u8 max2, u8 max3, u32 in) {
    tuple<u8, u8, u8> r;
    get<2>(r) = in % (max3 + 1);
    in /= (max3 + 1);
    get<1>(r) = in % (max2 + 1);
    in /= (max2 + 1);
    get<0>(r) = in;
    return r;
}

vector<u16> decode(vector<u16> maxValues, u64 in) {
    vector<u16> r(maxValues.size());
    for (u64 i = maxValues.size() - 1; i >= 1; --i) {
        u16 count = maxValues[i] + 1;
        r[i] = in % count;
        in /= count;
    }
    r[0] = in;
    return r;
}

vector<u16> decode(u8 size, u16 maxValue, u64 in) {
    vector<u16> r(size);
    u16 count = maxValue + 1;
    for (u64 i = size - 1; i >= 1; --i) {
        r[i] = in % count;
        in /= count;
    }
    r[0] = in;
    return r;
}

u64 encode(vector<u16> maxValues, vector<u16> values) {
    assert(maxValues.size() == values.size());
    u64 n = values.size();
    u64 factor = 1;
    u64 r = 0;
    for (int i = n - 1; i >= 0; --i) {
        u64 encodedValue = values[i];
        encodedValue *= factor;
        factor *= maxValues[i] + 1;
        r += encodedValue;
    }
    return r;
}

u64 encode(u16 maxValue, vector<u16> values){
    u64 n = values.size();
    u64 factor = 1;
    u64 r = 0;
    for (int i = n - 1; i >= 0; --i) {
        u64 encodedValue = values[i];
        encodedValue *= factor;
        factor *= maxValue + 1;
        r += encodedValue;
    }
    return r;
}

// equivalent to log2(maxvalue+1)
// TODO: find MSVC equivalent builtin
u64 neededBits(u64 maxValue) {
    return 64 - __builtin_clzll(maxValue);
}

u64 neededBits(u8 size, u16 maxValue) {
    u64 count = (u64(maxValue) + 1);
    for (u8 i = 1; i < size; ++i) {
        count *= (u64(maxValue) + 1);
    }
    return neededBits(count - 1);
}

u64 neededBits(vector<u16> maxValues) {
    u64 count = maxValues.at(0) + 1;
    for (u64 i = 1; i < maxValues.size(); ++i) {
        count *= maxValues[i] + 1;
    }
    return neededBits(count - 1);
}

} // namespace bitpray
