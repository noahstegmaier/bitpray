#include "bitaccess.hpp"

#include <array>
#include <cassert>
#include <fmt/core.h>

using std::array;
using std::vector;

const std::array<u8, 9> leadinglut = {
    0b11111111, 0b01111111, 0b00111111, 0b00011111, 0b00001111,
    0b00000111, 0b00000011, 0b00000001, 0b00000000,
};

const std::array<u8, 9> endinglut = {
    0b11111111, 0b11111110, 0b11111100, 0b11111000, 0b11110000,
    0b11100000, 0b11000000, 0b10000000, 0b00000000,
};

u8 leadingzeroes(u8 count) {
    return leadinglut.at(count);
}

u8 endingzeroes(u8 count) {
    return endinglut.at(count);
}

bitpusher::bitpusher(vector<u8> &data, u64 offset) : data(data), offset(offset) {}

void bitpusher::operator()(u64 start, u64 end, u64 value) {
    u64 length = end - start + 1;
    u64 mask = (u64(1) << length) - 1;
    // first 8 bits may not be used because value might get shifted by 7 to the right to make
    // writing easier
    // 0xfe.. might work since it 7, just set it to 0xff to be sure
    assert((0xff00000000000000 & value) == 0);
    // do the -1 properly
    assert((value & (u64(-1) << length)) == 0);
    u8 startbits = 8 - (start & 0x7);
    u8 startEmptyBits = start & 0x7;
    u8 endbits = (end & 0x7) + 1;
    u8 endEmptyBits = 7 - (end & 0x7);
    // we need to set the bits that we will write to to 0,
    // if there are bytes between start and end they will be completly overwritten
    u8 startmask = endingzeroes(startbits);
    u8 endmask = leadingzeroes(endbits);

    // at this point we could check endianness and read bytes from here, probably easier and faster
    // than shifting
    value = (value & mask) << endEmptyBits;
    // no idea
    u64 calclength = length + endEmptyBits;
    // first (as in most significant) bit index of value we want to read
    // an u64 has the following offsets [7 6 5 4 3 2 1 0]
    u64 byteoffset = ((calclength + 7) & 0xfffffffffffffff8) - 8;

    u64 startbyte = (start & 0xfffffffffffffff8) >> 3;
    u64 endbyte = (end & 0xfffffffffffffff8) >> 3;
    for (u64 i = startbyte; i <= endbyte; ++i) {

        u8 temp = (value >> (byteoffset)) & 0xff;
        // fmt::print("{} {}\n", data[offset+i], temp);
        if (i == startbyte) {
            data[offset + i] &= startmask;
            data[offset + i] |= temp;
        }
        if (i == endbyte) {
            data[offset + i] &= endmask;
            data[offset + i] |= temp;
        }
        if (i != startbyte && i != endbyte) {
            data[offset + i] = temp;
        }
        // fmt::print("{}\n", data[offset+i]);
        byteoffset -= 8;
    }
}

u64 bitpusher::operator()(u64 start, u64 end) const {
    u64 startbyte = (start & 0xfffffffffffffff8) >> 3;
    u64 endbyte = (end & 0xfffffffffffffff8) >> 3;
    // bits that are part of the data at the start of the last byte read
    u8 endbits = (end & 0x7) + 1;
    // unused 0 bits at the start of the first byte read
    u8 startEmptyBits = start & 0x7;
    // unused 0 bits at the end of the last byte read
    u8 endEmptyBits = 7 - (end & 0x7);
    u8 startmask = leadingzeroes(startEmptyBits);
    u8 endmask = endingzeroes(endEmptyBits);
    u64 ret = 0;
    u64 length = end - start + 1;
    assert(length <= 64);
    u64 toShift = end - (start & 0xfffffffffffffff8) + 1;
    // doesn't matter if we set 8 to 8 or substract 8
    if (toShift <= 8) {
        toShift = 0;
    } else {
        toShift -= 8;
    }
    for (u64 i = startbyte; i <= endbyte; ++i) {
        u64 temp = data[offset + i];
        if (i == startbyte) {
            temp = temp & startmask;
        }
        if (i == endbyte) {
            temp = temp & endmask;
            temp = temp >> endEmptyBits;
        }
        ret |= temp << toShift;
        if (i == endbyte - 1) {
            toShift -= endbits;
            assert(toShift == 0);
        } else {
            toShift -= 8;
        }
    }
    return ret;
}

void bitpusher::print() const {
    u64 i = 0;
    while (i < data.size()) {
        fmt::print("{:08b}", data[offset + i]);
        if ((i & 7) == 7) {
            fmt::print("\n");
        } else {
            fmt::print(" ");
        }
        ++i;
    }
    if ((i & 7) != 7) {
        fmt::print("\n");
    }
}

bitpuller::bitpuller(const vector<u8> &data, u64 offset) : data(data), offset(offset) {}

// TODO less code duplication maaybe?
u64 bitpuller::operator()(u64 start, u64 end) const {
    u64 startbyte = (start & 0xfffffffffffffff8) >> 3;
    u64 endbyte = (end & 0xfffffffffffffff8) >> 3;
    // bits that are part of the data at the start of the last byte read
    u8 endbits = (end & 0x7) + 1;
    // unused 0 bits at the start of the first byte read
    u8 startEmptyBits = start & 0x7;
    // unused 0 bits at the end of the last byte read
    u8 endEmptyBits = 7 - (end & 0x7);
    u8 startmask = leadingzeroes(startEmptyBits);
    u8 endmask = endingzeroes(endEmptyBits);
    u64 ret = 0;
    u64 length = end - start + 1;
    assert(length <= 64);
    u64 toShift = end - (start & 0xfffffffffffffff8) + 1;
    // doesn't matter if we set 8 to 8 or substract 8
    if (toShift <= 8) {
        toShift = 0;
    } else {
        toShift -= 8;
    }
    for (u64 i = startbyte; i <= endbyte; ++i) {
        u64 temp = data[offset + i];
        if (i == startbyte) {
            temp = temp & startmask;
        }
        if (i == endbyte) {
            temp = temp & endmask;
            temp = temp >> endEmptyBits;
        }
        ret |= temp << toShift;
        if (i == endbyte - 1) {
            toShift -= endbits;
            assert(toShift == 0);
        } else {
            toShift -= 8;
        }
    }
    return ret;
}

void bitpuller::print() const {
    u64 i = 0;
    while (i < data.size()) {
        fmt::print("{:08b}", data[offset + i]);
        if ((i & 7) == 7) {
            fmt::print("\n");
        } else {
            fmt::print(" ");
        }
        ++i;
    }
    if ((i & 7) != 7) {
        fmt::print("\n");
    }
}
